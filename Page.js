import React, { Component } from 'react';
import { View, Text , Button} from 'react-native';
import { observer , inject} from 'mobx-react';

@inject('BaseStore')
@observer
class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name:''
    };
  }


  componentDidMount = () => {
        this.setState({name:this.props.BaseStore.infoStore.name})
        
  };
  render() {
    return (
      <View>
        <Text> Page : {this.state.name} </Text>
        <Button 
          title="Change Page"
          onPress={() => this.props.navigation.navigate('App')}
        />
      </View>
    );
  }
}

export default Page;
