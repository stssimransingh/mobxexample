import React, { Component } from 'react';
import { View, Text , Button, TextInput} from 'react-native';
import {createSwitchNavigator,createAppContainer} from 'react-navigation';
import Page from './Page';

import { observer , inject, Provider} from 'mobx-react';


@inject('BaseStore')
@observer
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:''
    };
  }
  async updatedata(){
    alert('WORK');
    await this.props.BaseStore.infoStore.setName(this.state.name);

  }


  render() {
    return (

      <View>
        
        <Text> App </Text>
        <TextInput 
          value={this.state.name}
          onChangeText={(text) => this.setState({name : text})}
          placeholder="Enter Name"
        />
        <Button 
          title="Save Data"
          onPress={() => this.updatedata()}
        />
        <Button 
          title="Change Page"
          onPress={() => this.props.navigation.navigate('Page')}
        />
      </View>
    );
  }
}
export default class App2 extends React.Component{
  render(){
    return  <Contain />
  }
}
const navi = createSwitchNavigator({
  App,
  Page
})
const Contain = createAppContainer(navi);
