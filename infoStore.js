import { observable, action } from "mobx";

class InfoStore{
    @observable name = '';

    @action setName = (name) => {
        this.name = name;
    }
}

export default InfoStore;