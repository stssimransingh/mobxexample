import InfoStore from './infoStore';

class BaseStore{
    constructor(){
        this.infoStore = new InfoStore(this);
    }
}

export default BaseStore;